#!/bin/bash

mpirun -np 7 ~/phylobayes/pbmpi/data/pb_mpi -d simion2017_reordered_bottom_10k.phy -x 1 501 -cat -gtr simion2017_bottom_10k

mpirun -np 7 ~/phylobayes/pbmpi/data/pb_mpi -d simion2017_reordered_top_10k.phy -x 1 501 -cat -gtr simion2017_top_10k

