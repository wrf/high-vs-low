# high-vs-low #

This was a test of whether gene occupancy affects tree topology. The highest 10k-sites-worth, 30k, and lowest occupied 10k and 30k (appx) genes were selected. Possibly a better test would be to remove genes from the highest coverage dataset, and might be implemented later.

Overall it appears that the tree is mostly unchanged, where in the lowest occupancy case, *Pennatula rubra*, with under 10 genes out of ~120, still is in the correct position. The branch lengths are almost doubled when data is missing, likely impacting molecular clock analyses or positive selection.

The original dataset from [Simion et al 2017](https://doi.org/10.1016/j.cub.2017.02.031) can be found [here](https://github.com/psimion/SuppData_Metazoa_2017).

## [Data files](https://bitbucket.org/wrf/high-vs-low/downloads/) ##
* [datasets.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/datasets.tar.gz), the 4 datasets as .aln and .phy, partition files, and occupancy matrices
* [pbmpi_top_10k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/pbmpi_top_10k.tar.gz), [phylobayes](https://github.com/bayesiancook/pbmpi) results from the top 10k dataset
* [pbmpi_bottom_10k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/pbmpi_bottom_10k.tar.gz), phylobayes for the bottom 10k dataset
* [raxml_top_10k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/raxml_top_10k.tar.gz), [RAxML](https://github.com/stamatak/standard-RAxML) results for the top 10k dataset
* [raxml_top_30k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/raxml_top_30k.tar.gz), as above top 30k
* [raxml_bottom_10k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/raxml_bottom_10k.tar.gz), as above bottom 10k
* [raxml_bottom_30k.tar.gz](https://bitbucket.org/wrf/high-vs-low/downloads/raxml_bottom_30k.tar.gz), as above bottom 30k

![simion2017_reordered_top_30k_occ.png](https://bitbucket.org/wrf/high-vs-low/raw/b16b23bc8732ebdd142ff20c7c44e84e841ccb05/simion2017_reordered_top_30k_occ.png)

![simion2017_reordered_bottom_30k_occ.png](https://bitbucket.org/wrf/high-vs-low/raw/b16b23bc8732ebdd142ff20c7c44e84e841ccb05/simion2017_reordered_bottom_30k_occ.png)

## RAxML runs ##

```
~/raxml/standard-RAxML/bin/raxmlHPC-PTHREADS-SSE3-8.2.11 -f a -# 100 -m PROTGAMMAILG -x 1234 -p 1234 -T 6 -s simion2017_reordered_bottom_30k.phy -n simion2017_bottom_30k_v1

~/raxml/standard-RAxML/bin/raxmlHPC-PTHREADS-SSE3-8.2.11 -f a -# 100 -m PROTGAMMAILG -x 1234 -p 1234 -T 6 -s simion2017_reordered_top_30k.phy -n simion2017_top_30k_v1

~/raxml/standard-RAxML/bin/raxmlHPC-PTHREADS-SSE3-8.2.11 -f a -# 100 -m PROTGAMMAILG -x 1234 -p 1234 -T 6 -s simion2017_reordered_bottom_10k.phy -n simion2017_bottom_10k_v1

~/raxml/standard-RAxML/bin/raxmlHPC-PTHREADS-SSE3-8.2.11 -f a -# 100 -m PROTGAMMAILG -x 1234 -p 1234 -T 6 -s simion2017_reordered_top_10k.phy -n simion2017_top_10k_v1
```

High vs low directly, two trees overlaid, showing that major groups remain together, overall tree is the same, but some species change position. The major effect is the change in branch length.

![raxml_high_vs_low_overlay_v1.png](https://bitbucket.org/wrf/high-vs-low/raw/b16b23bc8732ebdd142ff20c7c44e84e841ccb05/raxml_high_vs_low_overlay_v1.png)

## phylobayes runs ##
Chains are excluded, as files are large (appx. 500MB each).

```
mpirun -np 7 ~/phylobayes/pbmpi/data/pb_mpi -d simion2017_reordered_bottom_10k.phy -x 1 501 -cat -gtr simion2017_bottom_10k

mpirun -np 7 ~/phylobayes/pbmpi/data/pb_mpi -d simion2017_reordered_top_10k.phy -x 1 501 -cat -gtr simion2017_top_10k
```
